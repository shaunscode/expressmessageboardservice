import { ReqHandler } from "./models/ReqHandler.interface";
import { MongoClient, Cursor } from "mongodb";

export class DBHandler implements ReqHandler {
    getBoard(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    getPost(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    createBoard(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    createPost(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    updateBoard(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    updatePost(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    deleteBoard(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    deletePost(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }
    
    getThread(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }

    
    base(inReq: any, inRes: any): void {
        inRes.sendFile(__dirname + '/info.html');
    }    
    getThreads(inReq: any, inRes: any): void {
        throw new Error("Method not implemented.");
    }

    getBoards(inReq: any, inRes: any): void {
        const client = this.getClient();
            
        //connect to db
        client.connect((inErr, inClient) => {
            if (inErr) {
                this.handleError(inRes, inErr);
                return;
            }

            //get the collection of boards
            const collection = inClient.db("blastdb").collection("boards");

            //query collection
            const cursor = collection.find();

            // populate response with results
            this.populateResponse(cursor, inRes);            
            
            //close the connection
            client.close();                
        });
    }

    getPosts(inReq: any, inRes: any): void {
        console.log("Method not implemented.");
    }

    getClient (): MongoClient {
        const MongoClient = require('mongodb').MongoClient;
        const uri = "mongodb+srv://admin:password01@blastdb-lgkkm.gcp.mongodb.net/test?retryWrites=true";
        return new MongoClient(uri, { useNewUrlParser: true });
    };

    handleError (inRes:any, inErr:any) {
        console.log("Error connecting to mongo: " + inErr);
        inRes.json({
            code: 2,
            content: inErr    
        });
        return;
    };

    populateResponse(inCursor: Cursor, inRes: any) {
        inCursor.toArray((inErr, inResults) => {
            if (inErr) {
                inRes.json({
                    code: 2, 
                    content: inErr
                });
            } else {
                inRes.json({
                    code: 0,
                    content: inResults
                });
            }
        })
    }

}