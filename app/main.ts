import express = require('express');
import cors = require('cors');
import bodyParser = require('body-parser');

import { MockHandler } from './MockHandler';
import { DBHandler } from './DBHandler';
import { ReqHandler } from './models/ReqHandler.interface';

export class BlastApi {

    isUsingMock = true;
    handler: ReqHandler;

    constructor() {

        if (this.isUsingMock) {
            this.handler = new MockHandler();
        } else {
            this.handler = new DBHandler();
        }

        // enable cross origin 
        const app = express();
        app.use(cors());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        
        app.get('/', (inReq: any, inRes:any) => {
            console.log('GET / hit');
            this.handler.base(inReq, inRes);
        });

        //GET
        app.get('/boards', (inReq: any, inRes: any) => {
            console.log('GET /boards hit');
            this.handler.getBoards(inReq, inRes);     
         });
 
         //get all boards to display
        app.get('/boards/:boardId', (inReq: any, inRes: any) => {
            console.log('GET /boards/:boardId hit');
            this.handler.getBoard(inReq, inRes);     
         });
         
         app.get('/posts', (inReq: any, inRes: any) => {
            console.log('GET /posts hit');
            this.handler.getPosts(inReq, inRes);
        });

        app.get('/posts/:postId', (inReq: any, inRes: any) => {
            console.log('GET /posts/:postId hit');
            this.handler.getPost(inReq, inRes);
        });
 




        app.get('/boards/:boardId/threads', (inReq: any, inRes: any) => {
            console.log('GET /boards/:boardId/threads hit');
            this.handler.getThreads(inReq, inRes);
        });

        app.get('/boards/:boardId/threads/:threadId', (inReq: any, inRes: any) => {
            console.log('GET /boards/:boardId/threads/:threadId hit');
            this.handler.getThread(inReq, inRes);
        });




        app.put('/boards/:boardId', (inReq: any, inRes: any) => {
            console.log('PUT /boards/:boardId hit');
            this.handler.updateBoard(inReq, inRes);
        });
        
        app.put('/posts/:postId', (inReq: any, inRes: any) => {
            console.log('PUT /posts/:postId hit');
            this.handler.updatePost(inReq, inRes);
        });
        
        app.post('/boards/:boardId/threads/new', (inReq: any, inRes: any) => {
            console.log('POST /boards/:boardId/threads/new hit');
            this.handler.createPost(inReq, inRes);
        });

        app.post('/posts/new', (inReq: any, inRes: any) => {
            console.log('POST /posts/new hit');
            this.handler.createPost(inReq, inRes);
        });

        
        app.listen((process.env.PORT || 5000), () => console.log(`Example app listening on port ${(process.env.PORT || 5000)}!`));
        //app.listen(5000, '10.245.79.43'), () => console.log(`Example app listening on port ${(process.env.PORT || 5000)}!`);

     }    

}

// need to create the obj so constructor start listener
// note: Heroku executes 'node ./build/main.js', rather than an ng serve, to get its projects running
new BlastApi();