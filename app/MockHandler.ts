import { ReqHandler } from "./models/ReqHandler.interface";
import { Data } from './MockData';
import { Post } from './models/post.interface';
import { Board } from "./models/board.interface";

export class MockHandler implements ReqHandler {
   

    base(inReq: any, inRes: any): void {
        inRes.sendFile(__dirname + '/info.html');
    }

    data = new Data();

    getBoards(inReq: any, inRes: any) {
        inRes.json({
            code: 0,
            content: this.data.boards
      });
    }    

    getBoard(inReq: any, inRes: any) {
        const board = this.data.boards.find(b => b._id === inReq.params.boardId);
        if (!board) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }

       // const posts = this.data.posts.filter(post => board.posts.includes(post._id));

        inRes.json({
            code: 0,
            content: board
        });
    }


    getPosts(inReq: any, inRes: any) {
        inRes.json({
            code: 0,
            content: this.data.posts
      });
    }    

    getPost(inReq: any, inRes: any) {
        const post = this.data.posts.find(b => b._id === inReq.params.postId);
        if (!post) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }

        inRes.json({
            code: 0,
            content: post
        });
    }



    getThreads(inReq: any, inRes: any) {
        const board = this.data.boards.find(b => b._id === inReq.params.boardId);
        if (!board) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }

        const posts = this.data.posts.filter(post => board.threads.includes(post._id));

        inRes.json({
            code: 0,
            content: posts
        });
        return;
    }

    getThread(inReq: any, inRes: any) {
        const post = this.data.posts.find(p => p._id === inReq.params.threadId);
        if (!post) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
       }

       // recursive'ish loop to get all children of the head post of the thread
        let posts = [post];
        let i = 0;
        while(i != posts.length) {
            posts[i].replies.forEach(replyId => {
                let reply = this.data.posts.find(p => p._id === replyId);
                if (reply) {
                    posts.push(reply);
                }
            }) 
            i++;
        }

        inRes.json({
            code: 0,
            content: posts
        });
        return;
    }





   createPost(inReq: any, inRes: any) {
        const post = inReq.body as Post;
        post._id = this.getMockId();
        this.data.posts.push(post);
        inRes.send(post);
    }

    createBoard(inReq: any, inRes: any) {
        const board = inReq.body as Board;
        board._id = this.getMockId();
        this.data.boards.push(board);
        inRes.send(board);
    }


    updateBoard(inReq: any, inRes: any): void {
        console.log("MockHandler.updateBoard() hit");
        const board = inReq.body as Board;
        if (board) {
            this.data.boards = this.data.boards.filter(b => b._id !== board._id);
            console.log("board was found..");
            this.data.boards.push(board);
            inRes.send(board);
        }else {
            console.log("no board to update found in the req body.");
        }
    }

    updatePost(inReq: any, inRes: any): void {
        const post = inReq.body as Post;
        
        console.log("MockHandler.updatePost() hit (obj: " + JSON.stringify(post));
        if (post) {
            console.log("post was found..");
            this.data.posts = this.data.posts.filter(b => b._id !== post._id);
            this.data.posts.push(post);
            inRes.send(post);
        } else {
            console.log("no post to update found in the req body.");
        }
    }

    deleteBoard(inReq: any, inRes: any): void {
        console.log("Method deleteBoard not implemented.");
    }
    deletePost(inReq: any, inRes: any): void {
        console.log("Method deletePost not implemented.");
    }



    getMockId (): string {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
        for (var i = 0; i < 24; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
};
