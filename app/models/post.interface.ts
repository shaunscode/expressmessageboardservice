import { Entity } from './entity.interface';

export interface Post extends Entity {
  modified?: string;
  parentId: string;
  content: string;
  replies: string[];
}
