import { Entity } from './entity.interface';

export interface Board extends Entity {
  threads: string[];
}
