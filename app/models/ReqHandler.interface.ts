export interface ReqHandler {

    base(inReq: any, inRes: any): void; //handle base dir call

    //calls to see all content
    getBoards(inReq: any, inRes: any): void;    //get all boards (eg the boards collection)
    getPosts(inReq: any, inRes: any): void;  

    //basic crud ops for data sets
    getBoard(inReq: any, inRes: any): void;
    getPost(inReq: any, inRes: any): void;
    
    createBoard (inReq: any, inRes: any): void;
    createPost (inReq: any, inRes: any): void;
    
    updateBoard (inReq: any, inRes: any): void;
    updatePost (inReq: any, inRes: any): void;

    deleteBoard (inReq: any, inRes: any): void;
    deletePost (inReq: any, inRes: any): void;


    //bulk retrieval operations 
    getThreads(inReq: any, inRes: any): void;   //get all posts for a specific board id
    getThread(inReq: any, inRes: any): void;     //get all posts for a specific thread (inclues head post)

}