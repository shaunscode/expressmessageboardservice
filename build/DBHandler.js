"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DBHandler = /** @class */ (function () {
    function DBHandler() {
    }
    DBHandler.prototype.getBoard = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.getPost = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.createBoard = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.createPost = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.updateBoard = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.updatePost = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.deleteBoard = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.deletePost = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.getThread = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.base = function (inReq, inRes) {
        inRes.sendFile(__dirname + '/info.html');
    };
    DBHandler.prototype.getThreads = function (inReq, inRes) {
        throw new Error("Method not implemented.");
    };
    DBHandler.prototype.getBoards = function (inReq, inRes) {
        var _this = this;
        var client = this.getClient();
        //connect to db
        client.connect(function (inErr, inClient) {
            if (inErr) {
                _this.handleError(inRes, inErr);
                return;
            }
            //get the collection of boards
            var collection = inClient.db("blastdb").collection("boards");
            //query collection
            var cursor = collection.find();
            // populate response with results
            _this.populateResponse(cursor, inRes);
            //close the connection
            client.close();
        });
    };
    DBHandler.prototype.getPosts = function (inReq, inRes) {
        console.log("Method not implemented.");
    };
    DBHandler.prototype.getClient = function () {
        var MongoClient = require('mongodb').MongoClient;
        var uri = "mongodb+srv://admin:password01@blastdb-lgkkm.gcp.mongodb.net/test?retryWrites=true";
        return new MongoClient(uri, { useNewUrlParser: true });
    };
    ;
    DBHandler.prototype.handleError = function (inRes, inErr) {
        console.log("Error connecting to mongo: " + inErr);
        inRes.json({
            code: 2,
            content: inErr
        });
        return;
    };
    ;
    DBHandler.prototype.populateResponse = function (inCursor, inRes) {
        inCursor.toArray(function (inErr, inResults) {
            if (inErr) {
                inRes.json({
                    code: 2,
                    content: inErr
                });
            }
            else {
                inRes.json({
                    code: 0,
                    content: inResults
                });
            }
        });
    };
    return DBHandler;
}());
exports.DBHandler = DBHandler;
