"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MockData_1 = require("./MockData");
var MockHandler = /** @class */ (function () {
    function MockHandler() {
        this.data = new MockData_1.Data();
    }
    MockHandler.prototype.base = function (inReq, inRes) {
        inRes.sendFile(__dirname + '/info.html');
    };
    MockHandler.prototype.getBoards = function (inReq, inRes) {
        inRes.json({
            code: 0,
            content: this.data.boards
        });
    };
    MockHandler.prototype.getBoard = function (inReq, inRes) {
        var board = this.data.boards.find(function (b) { return b._id === inReq.params.boardId; });
        if (!board) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }
        // const posts = this.data.posts.filter(post => board.posts.includes(post._id));
        inRes.json({
            code: 0,
            content: board
        });
    };
    MockHandler.prototype.getPosts = function (inReq, inRes) {
        inRes.json({
            code: 0,
            content: this.data.posts
        });
    };
    MockHandler.prototype.getPost = function (inReq, inRes) {
        var post = this.data.posts.find(function (b) { return b._id === inReq.params.postId; });
        if (!post) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }
        inRes.json({
            code: 0,
            content: post
        });
    };
    MockHandler.prototype.getThreads = function (inReq, inRes) {
        var board = this.data.boards.find(function (b) { return b._id === inReq.params.boardId; });
        if (!board) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }
        var posts = this.data.posts.filter(function (post) { return board.threads.includes(post._id); });
        inRes.json({
            code: 0,
            content: posts
        });
        return;
    };
    MockHandler.prototype.getThread = function (inReq, inRes) {
        var _this = this;
        var post = this.data.posts.find(function (p) { return p._id === inReq.params.threadId; });
        if (!post) {
            inRes.json({
                code: 1,
                content: []
            });
            return;
        }
        // recursive'ish loop to get all children of the head post of the thread
        var posts = [post];
        var i = 0;
        while (i != posts.length) {
            posts[i].replies.forEach(function (replyId) {
                var reply = _this.data.posts.find(function (p) { return p._id === replyId; });
                if (reply) {
                    posts.push(reply);
                }
            });
            i++;
        }
        inRes.json({
            code: 0,
            content: posts
        });
        return;
    };
    MockHandler.prototype.createPost = function (inReq, inRes) {
        var post = inReq.body;
        post._id = this.getMockId();
        this.data.posts.push(post);
        inRes.send(post);
    };
    MockHandler.prototype.createBoard = function (inReq, inRes) {
        var board = inReq.body;
        board._id = this.getMockId();
        this.data.boards.push(board);
        inRes.send(board);
    };
    MockHandler.prototype.updateBoard = function (inReq, inRes) {
        console.log("MockHandler.updateBoard() hit");
        var board = inReq.body;
        if (board) {
            this.data.boards = this.data.boards.filter(function (b) { return b._id !== board._id; });
            console.log("board was found..");
            this.data.boards.push(board);
            inRes.send(board);
        }
        else {
            console.log("no board to update found in the req body.");
        }
    };
    MockHandler.prototype.updatePost = function (inReq, inRes) {
        var post = inReq.body;
        console.log("MockHandler.updatePost() hit (obj: " + JSON.stringify(post));
        if (post) {
            console.log("post was found..");
            this.data.posts = this.data.posts.filter(function (b) { return b._id !== post._id; });
            this.data.posts.push(post);
            inRes.send(post);
        }
        else {
            console.log("no post to update found in the req body.");
        }
    };
    MockHandler.prototype.deleteBoard = function (inReq, inRes) {
        console.log("Method deleteBoard not implemented.");
    };
    MockHandler.prototype.deletePost = function (inReq, inRes) {
        console.log("Method deletePost not implemented.");
    };
    MockHandler.prototype.getMockId = function () {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < 24; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    };
    return MockHandler;
}());
exports.MockHandler = MockHandler;
;
