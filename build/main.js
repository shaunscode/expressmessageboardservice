"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var MockHandler_1 = require("./MockHandler");
var DBHandler_1 = require("./DBHandler");
var BlastApi = /** @class */ (function () {
    function BlastApi() {
        var _this = this;
        this.isUsingMock = true;
        if (this.isUsingMock) {
            this.handler = new MockHandler_1.MockHandler();
        }
        else {
            this.handler = new DBHandler_1.DBHandler();
        }
        // enable cross origin 
        var app = express();
        app.use(cors());
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.get('/', function (inReq, inRes) {
            console.log('GET / hit');
            _this.handler.base(inReq, inRes);
        });
        //GET
        app.get('/boards', function (inReq, inRes) {
            console.log('GET /boards hit');
            _this.handler.getBoards(inReq, inRes);
        });
        //get all boards to display
        app.get('/boards/:boardId', function (inReq, inRes) {
            console.log('GET /boards/:boardId hit');
            _this.handler.getBoard(inReq, inRes);
        });
        app.get('/posts', function (inReq, inRes) {
            console.log('GET /posts hit');
            _this.handler.getPosts(inReq, inRes);
        });
        app.get('/posts/:postId', function (inReq, inRes) {
            console.log('GET /posts/:postId hit');
            _this.handler.getPost(inReq, inRes);
        });
        app.get('/boards/:boardId/threads', function (inReq, inRes) {
            console.log('GET /boards/:boardId/threads hit');
            _this.handler.getThreads(inReq, inRes);
        });
        app.get('/boards/:boardId/threads/:threadId', function (inReq, inRes) {
            console.log('GET /boards/:boardId/threads/:threadId hit');
            _this.handler.getThread(inReq, inRes);
        });
        app.put('/boards/:boardId', function (inReq, inRes) {
            console.log('PUT /boards/:boardId hit');
            _this.handler.updateBoard(inReq, inRes);
        });
        app.put('/posts/:postId', function (inReq, inRes) {
            console.log('PUT /posts/:postId hit');
            _this.handler.updatePost(inReq, inRes);
        });
        app.post('/boards/:boardId/threads/new', function (inReq, inRes) {
            console.log('POST /boards/:boardId/threads/new hit');
            _this.handler.createPost(inReq, inRes);
        });
        app.post('/posts/new', function (inReq, inRes) {
            console.log('POST /posts/new hit');
            _this.handler.createPost(inReq, inRes);
        });
        app.listen((process.env.PORT || 5000), function () { return console.log("Example app listening on port " + (process.env.PORT || 5000) + "!"); });
        //app.listen(5000, '10.245.79.43'), () => console.log(`Example app listening on port ${(process.env.PORT || 5000)}!`);
    }
    return BlastApi;
}());
exports.BlastApi = BlastApi;
// need to create the obj so constructor start listener
// note: Heroku executes 'node ./build/main.js', rather than an ng serve, to get its projects running
new BlastApi();
