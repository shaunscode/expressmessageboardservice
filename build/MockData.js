"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Data = /** @class */ (function () {
    function Data() {
        this.boards = [
            {
                "_id": "5c8e0bb5e5b2934de03bd110",
                "title": "Board one",
                "created": "2019-01-01T14:00:00.000Z",
                "threads": ["5c8e0f52e5b2934de03bd112", "5c8e119ee5b2934de03bd114"]
            },
            {
                "_id": "5c8e0bb5e5b2934de03bd122",
                "title": "Board 2",
                "created": "2019-01-01T14:00:00.000Z",
                "threads": []
            }
        ];
        this.posts = [
            {
                "_id": "5c8e0f52e5b2934de03bd112",
                "parentId": "5c8e0bb5e5b2934de03bd110",
                "title": "post one",
                "created": '2019-01-01T14:00:00.000Z',
                "replies": []
            },
            {
                "_id": "5c8e119ee5b2934de03bd114",
                "parentId": "5c8e0bb5e5b2934de03bd110",
                "title": "post two",
                "created": "2019-01-01T14:00:00.000Z",
                "replies": ["5c8e119ee5b2934de03bd115", "5c8e119ee5b2934de03bd116"]
            },
            {
                "_id": "5c8e119ee5b2934de03bd115",
                "parentId": "5c8e0bb5e5b2934de03bd110",
                "title": "reply post",
                "created": "2019-01-01T14:00:00.000Z",
                "replies": []
            },
            {
                "_id": "5c8e119ee5b2934de03bd116",
                "parentId": "5c8e0bb5e5b2934de03bd110",
                "title": "reply two",
                "created": "2019-01-01T14:00:00.000Z",
                "replies": []
            }
        ];
    }
    return Data;
}());
exports.Data = Data;
